# Final DeCharla

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Mari Carmen Fernández de Pedro
* Titulación: Ingeniería en Sistemas de Telecomunicación
* Cuenta en laboratorios: mfdez
* Cuenta URJC: mc.fernandezd.2017@alumnos.urjc.es
* Video básico (url): https://youtu.be/iKZQ0PKh-u0
* Video parte opcional (url): https://youtu.be/mif_did-BJg
* Despliegue (url): http://mcfernandezdp.pythonanywhere.com/
* Contraseñas: saropracticafinal, mc.fernandezd.2017
* Cuenta Admin Site: admin/saro1234

## Resumen parte obligatoria
* Al poner la url de mi página web aparece un formulario para iniciar sesión. Introducimos una de las dos contraseñas posibles y nos redirige a la página principal.
* En la página principal aparece un menú desde el que se puede acceder, mediante enlaces, a otras páginas. También aparecen las salas creadas hasta ese momento y un formulario para acceder a cualquiera de las salas creadas o para crear una nueva sala.
* En la página de cada sala aparecen los mensajes que fueron envidados hasta ese momento, ya sea en modo texto o en modo imagen. Dentro de ella tenemos un formulario para mandar más mensajes (modo texto) o más imágenes (mediante su URL).
* En la página dinámica de cada sala aparece el mismo contenido que en la página de una sala. Cada 30 segundos actualiza los mensajes.
* Para cada sala, tenemos un recurso donde el documento está servido en formato JSON y muestra el autor, el mensaje, la fecha y el tipo de cada mensaje enviado en esa sala.
* En la página de configuración aparecen dos formularios. El primero es para elegir el nombre con el que se ponen los mensajes (por defecto es Anónimo). El segundo es para cambiar el tipo de letra.
* En la página de ayuda aparece una breve explicación sobre el funcionamiento de la página.
* En la página de acceso al Admin Site aparece un formulario para iniciar sesión, introducimos el nombre de usuario y la contraseña y nos redirige a la página de administración de Django.

## Lista partes opcionales

* Inclusión de un favicon del sitio. Es el icono de la página web.
* Cerrar sesión. En la página principal, en el menú, aparece la opción de cerrar sesión que nos redirige a la página de inicio de sesión donde se encuentra el formulario para introducir la contraseña de nuevo.
