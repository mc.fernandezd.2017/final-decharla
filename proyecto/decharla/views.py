from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Clave, Sesion, Sala, Mensajes, Visitas
import random
import string
from django.views.decorators.csrf import csrf_exempt
from django.template import loader


def login(request):  # inicio de sesión
    if request.method == "GET":
        template = loader.get_template('login.html')
        contexto = {}

        return HttpResponse(template.render(contexto, request))
    elif request.method == "POST":
        contrasenya = request.POST['contrasenya']
        contrasenya_filter = Clave.objects.filter(clave=contrasenya)
        if not contrasenya_filter or len(contrasenya) == 0:
            template = loader.get_template('login.html')
            contexto = {'len_contrasenya': len(contrasenya), 'contrasenya_filter': contrasenya_filter}

            return HttpResponse(template.render(contexto, request))
        else:
            template = loader.get_template('index.html')
            contexto = {}
            respuesta = HttpResponseRedirect("/", template.render(contexto, request))

            id_cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))  # identificador para la sesión
            respuesta.set_cookie('session', id_cookie, max_age=365 * 24 * 60 * 60)
            content = Sesion(sesion=id_cookie, autor="Anónimo")
            content.save()
            return respuesta


@csrf_exempt
def index(request):  # pagina principal
    salas = Sala.objects.all()
    mensajestxt = Mensajes.objects.filter(tipo='txt').count()
    mensajesimg = Mensajes.objects.filter(tipo='img').count()
    valor = request.COOKIES.get('session')  # se obtiene el valor de la cookie llamada 'session' de la solicitud
    id = Sesion.objects.filter(sesion=valor) # el valor anterior se utiliza para buscar una instancia del modelo Sesión que coincida con el identificador de sesión.
    if id:  # si coincide (se encuentra una instacia de Sesión)
        if request.method == "POST":
            name_sala = request.POST['sala']
            try:
                sala = Sala.objects.get(nombre=name_sala)
            except Sala.DoesNotExist:
                sala = Sala(nombre=name_sala)
                sala.save()
            return redirect('/' + name_sala + '/')
        else:
            num_salas = 0
            n_sala = {}
            mensajes_sin_leer = {}
            visitas = []
            sesion = None
            for sala in salas:
                sesion = Sesion.objects.get(sesion=valor)
                visitas = Visitas.objects.filter(sesion=sesion)
                num_salas = num_salas + 1
                num_mensajes = Mensajes.objects.filter(sala=sala).count()
                if num_mensajes > 0:
                    n_sala[sala.nombre] = num_mensajes
            for ultima_visita in visitas:
                fecha = ultima_visita.fecha
                sala = ultima_visita.sala
                mensajes = Mensajes.objects.filter(sala=sala)
                no_leidos = mensajes.filter(fecha__gt=fecha).count()
                mensajes_sin_leer[sala.nombre] = no_leidos
            template = loader.get_template('index.html')
            contexto = {'salas': n_sala, 'num_salas': num_salas, 'mensajes_sin_leer': mensajes_sin_leer, 'sesion': sesion, 'txt': mensajestxt, 'img': mensajesimg}

            return HttpResponse(template.render(contexto, request))
    else:  # si no coincide
        x = login(request)
        return x


@csrf_exempt
def configuracion(request):
    valor = request.COOKIES.get('session')
    id = Sesion.objects.filter(sesion=valor)
    if id:
        sesion = Sesion.objects.get(sesion=valor)
        if request.method == "GET":
            template = loader.get_template('configuration.html')
            contexto = {'autor': sesion.autor}

            return HttpResponse(template.render(contexto, request))
        elif request.method == "POST":
            sesion.autor = request.POST['autor']
            tipo = request.POST['tipo']
            sesion.save()
            template = loader.get_template('configuration.html')
            contexto = {'autor': sesion.autor, 'tipo': tipo}

            return HttpResponse(template.render(contexto, request))
    else:
        return HttpResponseRedirect('/')


def ayuda(request):
    valor = request.COOKIES.get('session')
    id = Sesion.objects.filter(sesion=valor)
    if id:
        template = loader.get_template('help.html')
        contexto = {}
        return HttpResponse(template.render(contexto, request))
    else:
        return HttpResponseRedirect('/')


@csrf_exempt
def sala(request, sala):
    valor = request.COOKIES.get('session')
    id = Sesion.objects.filter(sesion=valor)
    if id:
        mensajes = Mensajes.objects.all()
        n_sala = Sala.objects.get(nombre=sala)
        sesion = Sesion.objects.get(sesion=valor)
        filter_sala = Mensajes.objects.filter(sala=n_sala)
        filter_visit = Visitas.objects.filter(sala=n_sala, sesion=sesion)
        if request.method == "POST":
            if filter_visit:
                visita = Visitas.objects.get(sala=n_sala, sesion=sesion)
                visita.save()
            else:
                content = Visitas(sala=n_sala, sesion=sesion)
                content.save()
            message = request.POST['message']
            imagen = request.POST.get('images', False)
            if len(message) > 0:
                if imagen == False:
                    content = Mensajes(mensaje=message, sesion=sesion, sala=n_sala, tipo='txt')
                    content.save()
                else:  # si imagen == True
                    content = Mensajes(mensaje=message, sesion=sesion, sala=n_sala, tipo='img')
                    content.save()
                template = loader.get_template('sala.html')
                contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

                return HttpResponseRedirect(".", template.render(contexto, request))
            else:
                template = loader.get_template('sala.html')
                contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

                return HttpResponseRedirect(".", template.render(contexto, request))
        elif request.method == "GET":
            if filter_visit:
                visita = Visitas.objects.get(sala=n_sala, sesion=sesion)
                visita.save()
            else:
                content = Visitas(sala=n_sala, sesion=sesion)
                content.save()
            template = loader.get_template('sala.html')
            contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

            return HttpResponse(template.render(contexto, request))
    else:
        return HttpResponseRedirect('/')


def logout(request):
    valor = request.COOKIES.get('session')
    sesion = Sesion.objects.get(sesion=valor)
    if sesion:
        sesion.sesion = '0'  # marca la sesión como inactiva o cerrada
        sesion.save()
    return HttpResponseRedirect('/')


def json_sala(request, sala):
    valor = request.COOKIES.get('session')
    id = Sesion.objects.filter(sesion=valor)
    if id:
        n_sala = Sala.objects.get(nombre=sala)
        mensajes = Mensajes.objects.filter(sala=n_sala)
        mensaje_dict_list = []
        for message in mensajes:
            mensaje_dict = {'autor': message.sesion.autor, 'mensaje': message.mensaje, 'fecha': message.fecha.now().strftime('%Y-%m-%d %H:%M:%S'), 'tipo': message.tipo}
            mensaje_dict_list.append(mensaje_dict)

        return JsonResponse(mensaje_dict_list, safe=False)
    else:
        return HttpResponseRedirect('/')


@csrf_exempt
def dinamica_sala(request, sala):
    valor = request.COOKIES.get('session')
    id = Sesion.objects.filter(sesion=valor)
    if id:
        mensajes = Mensajes.objects.all()
        n_sala = Sala.objects.get(nombre=sala)
        sesion = Sesion.objects.get(sesion=valor)
        filter_sala = Mensajes.objects.filter(sala=n_sala)
        filter_visit = Visitas.objects.filter(sala=n_sala, sesion=sesion)
        if request.method == "POST":
            if filter_visit:
                visita = Visitas.objects.get(sala=n_sala, sesion=sesion)
                visita.save()
            else:
                content = Visitas(sala=n_sala, sesion=sesion)
                content.save()
            message = request.POST['message']
            imagen = request.POST.get('images', False)
            if len(message) > 0:
                if imagen == False:
                    content = Mensajes(mensaje=message, sesion=sesion, sala=n_sala, tipo='txt')
                    content.save()
                else:
                    content = Mensajes(mensaje=message, sesion=sesion, sala=n_sala, tipo='img')
                    content.save()
                template = loader.get_template('sala_dinamica.html')
                contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

                return HttpResponseRedirect(".", template.render(contexto, request))
            else:
                template = loader.get_template('sala_dinamica.html')
                contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

                return HttpResponseRedirect(".", template.render(contexto, request))
        elif request.method == "GET":
            if filter_visit:
                visita = Visitas.objects.get(sala=n_sala, sesion=sesion)
                visita.save()
            else:
                content = Visitas(sala=n_sala, sesion=sesion)
                content.save()
            template = loader.get_template('sala_dinamica.html')
            contexto = {'messages': mensajes, 'sala': sala, 'name_sala': filter_sala, 'sesion': sesion}

            return HttpResponse(template.render(contexto, request))
    else:
        return HttpResponseRedirect('/')


