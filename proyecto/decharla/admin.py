from django.contrib import admin
from .models import Clave, Sesion, Sala, Mensajes, Visitas

# Register your models here.
admin.site.register(Clave)
admin.site.register(Sesion)
admin.site.register(Sala)
admin.site.register(Mensajes)
admin.site.register(Visitas)

