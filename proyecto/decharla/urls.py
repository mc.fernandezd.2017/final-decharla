from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('logout/', views.logout),
    path('config/', views.configuracion),
    path('ayuda/', views.ayuda),
    path('dinamico/<str:sala>/', views.dinamica_sala),
    path('json/<str:sala>/', views.json_sala),
    path('<str:sala>/', views.sala),
]

