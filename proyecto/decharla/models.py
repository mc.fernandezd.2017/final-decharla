from django.db import models
# Create your models here.


class Clave(models.Model):
    clave = models.TextField()  # contraseña

    def __str__(self):
        return self.clave


class Sesion(models.Model):
    autor = models.CharField(max_length=50)
    sesion = models.CharField(max_length=50)  # cookie

    def __str__(self):
        return str(self.sesion + ":" + self.autor)


class Sala(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Mensajes(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.PROTECT, null=True)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    mensaje = models.TextField()
    fecha = models.DateTimeField(auto_now=True)
    TIPO = [('txt', 'texto'), ('img', 'imagen')]
    tipo = models.CharField(max_length=3, choices=TIPO, default="")

    def __str__(self):
        return str(self.sala.nombre + ":" + self.mensaje + ":" + self.sesion.autor)


class Visitas(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.sala.nombre + ":" + self.sesion.autor)




